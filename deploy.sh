#!/bin/sh

set -e

docker build -t golang-messagebird:1.0.0 ./golang-messagebird
docker build -t prometheus-messagebird:1.0.0 ./prometheus-messagebird

kubectl create -f golang_deployment.yaml
kubectl create -f golang_service.yaml
kubectl create -f prometheus_deployment.yaml
kubectl create -f prometheus_service.yaml