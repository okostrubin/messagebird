# Platform Engineer Assignment

## Requirements

- Docker
- Kubernetes
- Terraform

## Deployment of Kubernetes cluster to Amazon Web Services EKS

Initialize a working directory containing Terraform configuration files:

`cd aws-eks-messagebird`

`terraform init`

Deploy Kubernetes cluster to Amazon Web Services EKS:

`terraform apply`

## Deployment of stack

Run:
`./deploy.sh`

Check status of deployment using `kubectl get deployments`, `kubectl get pods` and `kubectl get services` commands.

Testing examples:

`wget http://messagebird.com/homersimpson`

`wget http://messagebird.com/covilha`

Prometheus server:

`http://messagebird.com:9090/targets#job-golang-messagebird`

Metrics:

`golang_messagebird_homersimpson_uri_total_requests`

`golang_messagebird_covilha_uri_total_requests`


## Removing stack

Run:
`./remove.sh`

## Technologies

- Docker
- Kubernetes
- Terraform
- Prometheus
- Shell
- Golang
