package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

var homersimpson_uri_total_requests = 0
var covilha_uri_total_requests = 0

func main() {
	// use PORT environment variable, or default to 8080
	port := "8080"
	if fromEnv := os.Getenv("PORT"); fromEnv != "" {
		port = fromEnv
	}

	// register functions to handle requests
	server := http.NewServeMux()
	server.HandleFunc("/homersimpson", homersimpson)
	server.HandleFunc("/covilha", covilha)
	server.HandleFunc("/metrics", metrics)

	// start the web server on port and accept requests
	log.Printf("Server listening on port %s", port)
	err := http.ListenAndServe(":"+port, server)
	log.Fatal(err)
}

func covilha(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving request: %s", r.URL.Path)
	covilha_uri_total_requests++

	loc, _ := time.LoadLocation("Europe/Lisbon")
	t :=  time.Now().In(loc)
    	fmt.Fprintf(w, "Timezone: Europe/Covilha\n")
	fmt.Fprintf(w, "Local Time: %v\n", t.Format("Mon Jan _2 15:04:05 2006"))
}

func homersimpson(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving request: %s", r.URL.Path)
	homersimpson_uri_total_requests++
 	http.ServeFile(w, r, "homersimpson.gif")
}

func metrics(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving request: %s", r.URL.Path)
    	fmt.Fprintf(w, "golang_messagebird_homersimpson_uri_total_requests %d\n", homersimpson_uri_total_requests)
    	fmt.Fprintf(w, "golang_messagebird_covilha_uri_total_requests %d\n", covilha_uri_total_requests)
}