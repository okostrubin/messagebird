#!/bin/sh

set -e

kubectl delete deployment golang
kubectl delete service golang
kubectl delete deployment prometheus
kubectl delete service prometheus